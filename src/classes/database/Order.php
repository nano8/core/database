<?php

namespace laylatichy\nano\core\classes\database;

use laylatichy\nano\core\enums\database\OrderType;

final class Order {
    public function __construct(
        private readonly string $by,
        private readonly OrderType $type = OrderType::ASC,
    ) {
    }

    public function getBy(): string {
        return $this->by;
    }

    public function getType(): OrderType {
        return $this->type;
    }
}
