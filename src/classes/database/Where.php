<?php

namespace laylatichy\nano\core\classes\database;

use laylatichy\nano\core\enums\database\ComparisonType;
use laylatichy\nano\core\enums\database\WhereType;

final class Where {
    public function __construct(
        private string $left,
        private string $right,
        private ComparisonType $comparison,
        private WhereType $type = WhereType::WHERE,
    ) {
    }

    public function getLeft(): string {
        return $this->left;
    }

    public function setLeft(string $left): void {
        $this->left = $left;
    }

    public function getRight(): string {
        return $this->right;
    }

    public function setRight(string $right): void {
        $this->right = $right;
    }

    public function getComparison(): ComparisonType {
        return $this->comparison;
    }

    public function setComparison(ComparisonType $comparison): void {
        $this->comparison = $comparison;
    }

    public function getType(): WhereType {
        return $this->type;
    }

    public function setType(WhereType $type): void {
        $this->type = $type;
    }
}
