<?php

namespace laylatichy\nano\core\classes\database;

abstract class Connector {
    private string $host;

    private string $port;

    private string $user;

    private string $password;

    private bool   $enabled   = false;

    private bool   $connected = false;

    public function getHost(): string {
        return $this->host;
    }

    public function setHost(string $host): self {
        $this->host = $host;

        return $this;
    }

    public function getPort(): string {
        return $this->port;
    }

    public function setPort(string $port): self {
        $this->port = $port;

        return $this;
    }

    public function getUser(): string {
        return $this->user;
    }

    public function setUser(string $user): self {
        $this->user = $user;

        return $this;
    }

    public function getPassword(): string {
        return $this->password;
    }

    public function setPassword(string $password): self {
        $this->password = $password;

        return $this;
    }

    public function isEnabled(): bool {
        return $this->enabled;
    }

    public function enable(): self {
        $this->enabled = true;

        return $this;
    }

    public function isConnected(): bool {
        return $this->connected;
    }

    public function setConnected(bool $connected): self {
        $this->connected = $connected;

        return $this;
    }

    abstract public function connect(): self;

    abstract public function disconnect(): self;

    abstract public function insert(Query $query): Query;

    abstract public function update(Query $query): Query;

    abstract public function delete(Query $query): Query;

    abstract public function select(Query $query): Query;
}