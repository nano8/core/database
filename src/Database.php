<?php

namespace laylatichy\nano\core;

use laylatichy\nano\core\classes\database\Mysql;
use laylatichy\nano\core\classes\database\Params;
use laylatichy\nano\core\classes\database\Query;
use laylatichy\nano\core\classes\database\Redis;
use laylatichy\nano\core\classes\database\Where;
use laylatichy\nano\core\enums\database\ComparisonType;
use laylatichy\nano\core\enums\database\Methods;
use laylatichy\nano\core\enums\database\OrderType;
use laylatichy\nano\core\enums\database\WhereType;

final class Database {
    private static self $_instance;

    private function __construct(
        private readonly Mysql $master = new Mysql(),
        private readonly Mysql $read = new Mysql(),
        private readonly Redis $redis = new Redis(),
        private readonly Query $query = new Query(),
    ) {
    }

    public static function getInstance(): self {
        if (!isset(self::$_instance)) {
            self::$_instance = new self();

            self::loadConfig();
        }

        return self::$_instance;
    }

    public static function getMaster(): Mysql {
        return self::$_instance->master;
    }

    public static function getRead(): Mysql {
        return self::$_instance->read;
    }

    public static function getRedis(): Redis {
        return self::$_instance->redis;
    }

    public static function getQuery(): Query {
        return self::$_instance->query;
    }

    public function insert(string $table, array $data): self {
        $this->query->reset();

        $this->query->setTable($table);
        $this->query->setMethod(Methods::INSERT);
        $this->query->setColumns(array_keys($data));

        foreach ($data as $column => $value) {
            $this->query->addValues($column, is_null($value) ? null : (is_bool($value) ? (int)$value : $value));
            $this->query->addParams(new Params(":{$column}", is_null($value) ? null : (is_bool($value) ? (int)$value : $value)));
        }

        return $this;
    }

    public function update(string $table, array $data): self {
        $this->query->reset();

        $this->query->setTable($table);
        $this->query->setMethod(Methods::UPDATE);
        $this->query->setColumns(array_keys($data));

        foreach ($data as $column => $value) {
            $this->query->addValues($column, is_null($value) ? null : (is_bool($value) ? (int)$value : $value));
            $this->query->addParams(new Params(":{$column}", is_null($value) ? null : (is_bool($value) ? (int)$value : $value)));
        }

        return $this;
    }

    public function delete(string $table): self {
        $this->query->reset();

        $this->query->setTable($table);
        $this->query->setMethod(Methods::DELETE);

        return $this;
    }

    public function select(string $table, array $columns = []): self {
        $this->query->reset();

        $this->query->setTable($table);
        $this->query->setMethod(Methods::SELECT);
        $this->query->setColumns($columns);

        return $this;
    }

    public function where(string $left, int|string $right, ComparisonType $comparison = ComparisonType::EQUAL): self {
        $this->query->addWhere(new Where($left, (string)$right, $comparison, WhereType::WHERE));

        return $this;
    }

    public function and(string $left, int|string $right, ComparisonType $comparison = ComparisonType::EQUAL): self {
        $this->query->addWhere(new Where($left, (string)$right, $comparison, WhereType::AND));

        return $this;
    }

    public function or(string $left, int|string $right, ComparisonType $comparison = ComparisonType::EQUAL): self {
        $this->query->addWhere(new Where($left, (string)$right, $comparison, WhereType::OR));

        return $this;
    }

    public function limit(int $limit): self {
        $this->query->setLimit($limit);

        return $this;
    }

    public function offset(int $offset): self {
        $this->query->setOffset($offset);

        return $this;
    }

    public function order(string $by, OrderType $type = OrderType::ASC): self {
        $this->query->setOrder($by, $type);

        return $this;
    }

    public function execute(): int|bool|array {
        return match ($this->query->getMethod()) {
            Methods::INSERT => $this->_insert(),
            Methods::UPDATE => $this->_update(),
            Methods::DELETE => $this->_delete(),
            Methods::SELECT => $this->_select(),
            default         => throw new NanoException(message: 'invalid method call'),
        };
    }

    private function _insert(): int {
        $this->master->insert($this->query);

        $this->redis->insert($this->query);

        return $this->query->getId();
    }

    private function _update(): bool {
        $this->master->update($this->query);

        $this->redis->update($this->query);

        return $this->query->isUpdated();
    }

    private function _delete(): bool {
        $this->master->delete($this->query);

        $this->redis->delete($this->query);

        return $this->query->isDeleted();
    }

    private function _select(): array {
        $this->redis->select($this->query);

        if (!empty($this->query->getItems())) {
            return $this->query->getItems();
        }

        $this->read->isEnabled()
            ? $this->read->select($this->query)
            : $this->master->select($this->query);

        $this->redis->setItems($this->query);

        return $this->query->getItems();
    }

    private static function loadConfig(): void {
        $file = Config::getRoot() . '/../.config/.db.php';

        if (!file_exists($file)) {
            throw new NanoException('Database config file not found');
        }

        require $file;
    }
}
