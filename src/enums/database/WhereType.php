<?php

namespace laylatichy\nano\core\enums\database;

enum WhereType {
    case WHERE;

    case AND;

    case OR;
}
