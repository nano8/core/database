<?php

namespace laylatichy\nano\core\enums\database;

enum OrderType {
    case ASC;

    case DESC;
}
