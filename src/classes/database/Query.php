<?php

namespace laylatichy\nano\core\classes\database;

use laylatichy\nano\core\enums\database\Methods;
use laylatichy\nano\core\enums\database\OrderType;

class Query {
    private string  $table;

    private Methods $method;

    /**
     * @var string[]
     */
    private array $columns = [];

    private array $values  = [];

    /**
     * @var Params[]
     */
    private array $params = [];

    /**
     * @var Where[]
     */
    private array  $where  = [];

    private array  $items  = [];

    private int    $id     = 0;

    private bool   $updated;

    private bool   $deleted;

    private int    $offset = 0;

    private int    $limit  = 0;

    private ?Order $order  = null;

    private string $key    = '';

    public function getTable(): string {
        return $this->table;
    }

    public function setTable(string $table): self {
        $this->table = $table;

        $this->addKey($table);

        return $this;
    }

    public function getMethod(): Methods {
        return $this->method;
    }

    public function setMethod(Methods $method): self {
        $this->method = $method;

        return $this;
    }

    public function getColumns(): array {
        return $this->columns;
    }

    public function setColumns(array $columns = []): self {
        $this->columns = empty($columns) ? ['*'] : $columns;

        return $this;
    }

    public function getParams(): array {
        $params = [];

        foreach ($this->params as $param) {
            $params[$param->getName()] = $param->getValue();
        }

        return $params;
    }

    public function addParams(Params $param): self {
        $this->params[] = $param;

        return $this;
    }

    public function getValues(): array {
        return $this->values;
    }

    public function addValues(string $column, ?string $value): self {
        $this->values[$column] = $value;

        return $this;
    }

    public function getWhere(): array {
        return $this->where;
    }

    public function addWhere(Where $where): self {
        $this->where[] = $where;

        if ($where->getLeft() === "{$this->getTable()}_id") {
            $this->setId((int)$where->getRight());
        } else {
            $this->addKey(":{$where->getType()->name}");
            $this->addKey("-{$where->getLeft()}");
            $this->addKey("-{$where->getComparison()->name}");
            $this->addKey("-{$where->getRight()}");
        }

        return $this;
    }

    public function getId(): int {
        return $this->id;
    }

    public function setId(int $id): self {
        $this->addValues($this->getTable() . '_id', (string)$id);

        $this->id = $id;

        $this->addKey(":{$id}");

        return $this;
    }

    public function isUpdated(): bool {
        return $this->updated;
    }

    public function setUpdated(bool $updated): self {
        $this->updated = $updated;

        return $this;
    }

    public function isDeleted(): bool {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self {
        $this->deleted = $deleted;

        return $this;
    }

    public function getOffset(): int {
        return $this->offset;
    }

    public function setOffset(int $offset): self {
        $this->offset = $offset;

        $this->addKey(":offset-{$offset}");

        return $this;
    }

    public function getLimit(): int {
        return $this->limit;
    }

    public function setLimit(int $limit): self {
        $this->limit = $limit;

        $this->addKey(":limit-{$limit}");

        return $this;
    }

    public function getOrder(): ?Order {
        return $this->order;
    }

    public function setOrder(string $by, OrderType $type = OrderType::ASC): self {
        $this->order = new Order($by, $type);

        $this->addKey(':orderby');
        $this->addKey("-{$by}");
        $this->addKey("-{$type->name}");

        return $this;
    }

    public function getItems(): array {
        return $this->items;
    }

    public function setItems(array $items): self {
        $this->items = $items;

        return $this;
    }

    public function getKey(): string {
        return $this->key;
    }

    public function addKey(string $key): self {
        $this->key .= strtolower($key);

        return $this;
    }

    public function reset(): void {
        unset(
            $this->table,
            $this->method,
            $this->update,
        );

        $this->columns = [];
        $this->values  = [];
        $this->params  = [];
        $this->where   = [];
        $this->items   = [];
        $this->id      = 0;
        $this->limit   = 0;
        $this->offset  = 0;
        $this->order   = null;
        $this->key     = '';
    }
}