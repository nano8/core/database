### nano/core/database

[![release](https://img.shields.io/packagist/v/laylatichy/nano-core-database?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-database)
[![PHPStan](https://img.shields.io/badge/PHPStan-Level%20max-brightgreen.svg?style=flat&logo=php)](https://shields.io/#/)
[![Total Downloads](https://img.shields.io/packagist/dt/laylatichy/nano-core-database?style=flat-square)](https://packagist.org/packages/laylatichy/nano-core-database)

##### CI STATUS

-   dev [![pipeline status](https://gitlab.com/nano8/core/database/badges/dev/pipeline.svg)](https://gitlab.com/nano8/core/database/-/commits/dev)
-   master [![pipeline status](https://gitlab.com/nano8/core/database/badges/master/pipeline.svg)](https://gitlab.com/nano8/core/database/-/commits/master)
-   release [![pipeline status](https://gitlab.com/nano8/core/database/badges/release/pipeline.svg)](https://gitlab.com/nano8/core/database/-/commits/release)

#### Install

-   `composer require laylatichy/nano-core-database`
