<?php

namespace laylatichy\nano\core\classes\database;

use Exception;
use laylatichy\nano\core\HttpCode;
use laylatichy\nano\core\NanoException;
use PDO;
use PDOException;
use PDOStatement;

class Mysql extends Connector {
    private string $database;

    private string $prefix;

    /**
     * @var string[]
     */
    private array        $sql = [];

    private PDO          $connector;

    private PDOStatement $statement;

    public function getDatabase(): string {
        return $this->database;
    }

    public function setDatabase(string $database): self {
        $this->database = $database;

        return $this;
    }

    public function getPrefix(): string {
        return $this->prefix;
    }

    public function setPrefix(string $prefix): self {
        $this->prefix = $prefix;

        return $this;
    }

    public function setConnector(PDO $connector): void {
        $this->connector = $connector;
    }

    public function getConnector(): PDO {
        return $this->connector;
    }

    public function connect(): self {
        try {
            $this->setConnector(
                new PDO(
                    $this->getDsn(),
                    $this->getUser(),
                    $this->getPassword(),
                    [
                        PDO::ATTR_EMULATE_PREPARES   => true,
                        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    ],
                )
            );

            $this->setConnected(true);
        } catch (PDOException $e) {
            error_log('MySQL Exception:' . $e->getMessage());

            throw new NanoException(
                message: $e->getMessage(),
                code: HttpCode::INTERNAL_SERVER_ERROR->code(),
            );
        }

        return $this;
    }

    public function disconnect(): self {
        unset($this->connector, $this->statement);
        
        $this->setConnected(false);

        return $this;
    }

    public function insert(Query $query): Query {
        $this->addSql($query->getMethod()->name);
        $this->addSql(sprintf('INTO %s%s', $this->getPrefix(), $query->getTable()));
        $this->addSql(sprintf('( %s )', implode(', ', $query->getColumns())));
        $this->addSql(sprintf('VALUES ( %s )', implode(', ', array_keys($query->getParams()))));

        try {
            $this->connect()->prepare()->execute($query->getParams());

            $query->setId((int)$this->getConnector()->lastInsertId());

            $this->disconnect();
        } catch (Exception $e) {
            throw new NanoException(
                message: $e->getMessage(),
                code: HttpCode::INTERNAL_SERVER_ERROR->code(),
            );
        }

        return $query;
    }

    public function update(Query $query): Query {
        $this->addSql($query->getMethod()->name);
        $this->addSql($this->getPrefix() . $query->getTable());
        $this->addSql(
            sprintf(
                'SET %s',
                implode(', ', array_map(fn ($column): string => "{$column} = :{$column}", $query->getColumns()))
            )
        );

        /** @var Where $where */
        foreach ($query->getWhere() as $key => $where) {
            $this->addSql(
                sprintf(
                    '%s %s%s:where_%s',
                    $where->getType()->name,
                    $where->getLeft(),
                    $where->getComparison()->operator(),
                    $key,
                )
            );

            $query->addParams(new Params(':where_' . $key, $where->getRight()));
        }

        try {
            $query->setUpdated($this->connect()->prepare()->execute($query->getParams()));

            $this->disconnect();
        } catch (Exception $e) {
            throw new NanoException(
                message: $e->getMessage(),
                code: HttpCode::INTERNAL_SERVER_ERROR->code(),
            );
        }

        return $query;
    }

    public function delete(Query $query): Query {
        $this->addSql($query->getMethod()->name);
        $this->addSql(sprintf('FROM %s', $this->getPrefix() . $query->getTable()));

        /** @var Where $where */
        foreach ($query->getWhere() as $key => $where) {
            $this->addSql(
                sprintf(
                    '%s %s%s:where_%s',
                    $where->getType()->name,
                    $where->getLeft(),
                    $where->getComparison()->operator(),
                    $key,
                )
            );

            $query->addParams(new Params(':where_' . $key, $where->getRight()));
        }

        try {
            $query->setDeleted($this->connect()->prepare()->execute($query->getParams()));

            $this->disconnect();
        } catch (Exception $e) {
            throw new NanoException(
                message: $e->getMessage(),
                code: HttpCode::INTERNAL_SERVER_ERROR->code(),
            );
        }

        return $query;
    }

    public function select(Query $query): Query {
        $this->addSql($query->getMethod()->name);
        $this->addSql(implode(', ', $query->getColumns()));
        $this->addSql(sprintf('FROM %s', $this->getPrefix() . $query->getTable()));

        /** @var Where $where */
        foreach ($query->getWhere() as $key => $where) {
            $this->addSql(
                sprintf(
                    '%s %s%s:where_%s',
                    $where->getType()->name,
                    $where->getLeft(),
                    $where->getComparison()->operator(),
                    $key,
                )
            );

            $query->addParams(new Params(':where_' . $key, $where->getRight()));
        }

        if ($query->getOrder()) {
            $this->addSql(
                sprintf(
                    'ORDER BY %s %s',
                    $query->getOrder()->getBy(),
                    $query->getOrder()->getType()->name,
                )
            );
        }

        if ($query->getLimit()) {
            $this->addSql(sprintf('LIMIT %d', $query->getLimit()));

            if ($query->getOffset()) {
                $this->addSql(sprintf('OFFSET %d', $query->getOffset()));
            }
        }

        try {
            $this->connect()->prepare()->execute($query->getParams());

            $items = $this->getStatement()->fetchAll();

            $query->setItems($items);

            $this->disconnect();
        } catch (Exception $e) {
            throw new NanoException(
                message: $e->getMessage(),
                code: HttpCode::INTERNAL_SERVER_ERROR->code(),
            );
        }

        return $query;
    }

    private function getDsn(): string {
        return sprintf(
            'mysql:host=%s;port=%s;dbname=%s;charset=utf8mb4',
            $this->getHost(),
            $this->getPort(),
            $this->getDatabase(),
        );
    }

    private function getSql(): array {
        return $this->sql;
    }

    private function clearSql(): self {
        $this->sql = [];

        return $this;
    }

    private function addSql(string $sql): void {
        $this->sql[] = $sql;
    }

    private function getStatement(): PDOStatement {
        return $this->statement;
    }

    private function setStatement(PDOStatement $statement): self {
        $this->statement = $statement;

        return $this;
    }

    private function prepare(): PDOStatement {
        $this->setStatement($this->getConnector()->prepare(implode(' ', $this->getSql())))
            ->clearSql();

        return $this->getStatement();
    }
}