<?php

namespace laylatichy\nano\core\classes\database;

use Exception;
use laylatichy\nano\core\HttpCode;
use laylatichy\nano\core\NanoException;
use Predis\Client;

class Redis extends Connector {
    private Client $connector;

    private int    $ttl = 5;

    public function setConnector(Client $connector): void {
        $this->connector = $connector;
    }

    public function getConnector(): Client {
        return $this->connector;
    }

    public function getTtl(): int {
        return $this->ttl;
    }

    public function setTtl(int $seconds): self {
        $this->ttl = $seconds;

        return $this;
    }

    public function connect(): self {
        try {
            $this->setConnector(
                new Client([
                    'host' => $this->getHost(),
                    'port' => $this->getPort(),
                ])
            );

            $this->connector->auth($this->getPassword());

            $this->connector->connect();

            $this->setConnected(true);
        } catch (Exception $e) {
            error_log('Redis Exception:' . $e->getMessage());

            throw new NanoException(
                message: $e->getMessage(),
                code: HttpCode::INTERNAL_SERVER_ERROR->code(),
            );
        }

        return $this;
    }

    public function disconnect(): self {
        unset($this->connector);

        $this->setConnected(false);

        return $this;
    }

    public function setItems(Query $query): Query {
        if (!$this->isEnabled()) {
            return $query;
        }

        try {
            $this->connect();

            $data = $query->getId() && !empty($query->getItems())
                ? json_encode($query->getItems()[0])
                : json_encode($query->getItems());

            $this->getConnector()->set($query->getKey(), $data);
            $this->getConnector()->expire($query->getKey(), $this->getTtl());

            $this->disconnect();
        } catch (Exception $e) {
            throw new NanoException(
                message: $e->getMessage(),
                code: HttpCode::INTERNAL_SERVER_ERROR->code(),
            );
        }

        return $query;
    }

    public function insert(Query $query): Query {
        if (!$query->getId() || !$this->isEnabled()) {
            return $query;
        }

        try {
            $this->connect();

            $this->getConnector()->set($query->getKey(), json_encode($query->getValues()));
            $this->getConnector()->expire($query->getKey(), $this->getTtl());

            $this->disconnect();
        } catch (Exception $e) {
            throw new NanoException(
                message: $e->getMessage(),
                code: HttpCode::INTERNAL_SERVER_ERROR->code(),
            );
        }

        return $query;
    }

    public function update(Query $query): Query {
        if (!$query->getId() || !$this->isEnabled() || !$query->isUpdated()) {
            return $query;
        }

        try {
            $this->connect();

            $this->getConnector()->set($query->getKey(), json_encode($query->getValues()));
            $this->getConnector()->expire($query->getKey(), $this->getTtl());

            $this->disconnect();
        } catch (Exception $e) {
            throw new NanoException(
                message: $e->getMessage(),
                code: HttpCode::INTERNAL_SERVER_ERROR->code(),
            );
        }

        return $query;
    }

    public function delete(Query $query): Query {
        if (!$query->getId() || !$this->isEnabled() || !$query->isDeleted()) {
            return $query;
        }

        try {
            $this->connect()->getConnector()->del($query->getKey());

            $this->disconnect();
        } catch (Exception $e) {
            throw new NanoException(
                message: $e->getMessage(),
                code: HttpCode::INTERNAL_SERVER_ERROR->code(),
            );
        }

        return $query;
    }

    public function select(Query $query): Query {
        if (!$this->isEnabled()) {
            return $query;
        }

        $query->getId() ? $this->selectById($query) : $this->selectAll($query);

        return $query;
    }

    private function selectAll(Query $query): void {
        try {
            $this->connect();

            if ($this->getConnector()->exists($query->getKey())) {
                $data = $this->getConnector()->get($query->getKey());

                if ($data) {
                    $items = json_decode($data, true);

                    if (is_array($items)) {
                        $query->setItems($items);
                    }
                }
            }

            $this->disconnect();
        } catch (Exception $e) {
            throw new NanoException(
                message: $e->getMessage(),
                code: HttpCode::INTERNAL_SERVER_ERROR->code(),
            );
        }
    }

    private function selectById(Query $query): void {
        try {
            $this->connect();

            if ($this->getConnector()->exists($query->getKey())) {
                $data = $this->getConnector()->get($query->getKey());

                if ($data) {
                    $items = json_decode($data);

                    if (isset($items)) {
                        empty($items) ? $query->setItems([]) : $query->setItems([$items]);
                    }
                }
            }

            $this->disconnect();
        } catch (Exception $e) {
            throw new NanoException(
                message: $e->getMessage(),
                code: HttpCode::INTERNAL_SERVER_ERROR->code(),
            );
        }
    }
}
