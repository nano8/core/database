<?php

namespace laylatichy\nano\core\enums\database;

enum ComparisonType {
    case GREATER_THAN;

    case GREATER_THAN_OR_EQUAL;

    case LESS_THAN;

    case LESS_THAN_OR_EQUAL;

    case NOT_EQUAL;

    case NULL_SAFE_EQUAL;

    case EQUAL;

    case IS;

    case IS_NOT;

    case IS_NOT_NULL;

    case IS_NULL;

    case LIKE;

    case NOT_LIKE;

    public function operator(): string {
        return match ($this) {
            self::GREATER_THAN          => '>',
            self::GREATER_THAN_OR_EQUAL => '>=',
            self::LESS_THAN             => '<',
            self::LESS_THAN_OR_EQUAL    => '<=',
            self::NOT_EQUAL             => '!=',
            self::NULL_SAFE_EQUAL       => '<=>',
            self::EQUAL                 => '=',
            self::IS                    => 'IS',
            self::IS_NOT                => 'IS NOT',
            self::IS_NOT_NULL           => 'IS NOT NULL',
            self::IS_NULL               => 'IS NULL',
            self::LIKE                  => 'LIKE',
            self::NOT_LIKE              => 'NOT LIKE',
        };
    }

}
