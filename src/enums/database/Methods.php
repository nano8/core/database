<?php

namespace laylatichy\nano\core\enums\database;

enum Methods {
    case DESCRIBE;

    case UPDATE;

    case INSERT;

    case DELETE;

    case SELECT;
}